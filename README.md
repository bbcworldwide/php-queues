![BBC Store](https://store.bbc.com/sites/all/themes/barcelona/images/Logo-BBC.png)

BBC Worldwide - Queues
===

This library provides with an easy, simplified way to publish and consume queues. At the moment, only AWS SNS and SQS 
are implemented. SNS obviously supports publishing only.

Installation
---

Preferred installation method is via composer. Since we're not published in Packagist, you'll need a little extra
config in your composer.json.

First, add an entry to the `repositories` key in composer.json:
```json
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://bitbucket.org/bbcworldwide/php-queues.git"
    }
  ],

```

Second, run `composer require bbcworldwide/php-queues`.

That's it.

Example:

```json
{
  "name": "some/project",
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://bitbucket.org/bbcworldwide/php-queues.git"
    }
  ],
  "require":      {
    "php": ">=5.6.0",
    "bbcworldwide/php-queues": "^1.0"
  }
}
```

How to
---

Head off to the [examples](examples) folder for examples on a [publisher](examples/publisher.php) and a [consumer](examples/consumer.php). 
Queue client is instantiated on [makeQueueClient.php](examples/makeQueueClient.php).

You can run these examples from the command line, simply copy `examples/awsCredentials.php.dist` into 
`examples/awsCredentials.php` and fill in your AWS credentials where appropriate.

Your client application MUST provide with implementations for both the [Serializer](src/Client/ClientInterface.php) 
as well as the [Message](src/Message/MessageInterface.php) and supply the Serializer to whichever driver's constructor. 
The `deserialize` method of your serializer must return the same message type.

You can, optionally, provide with a psr logger, since the queue client interface is also LoggerAware. Simply pass on
your own logger instance to the client.

To cut a long story short:
  * Install the library via composer (see installation above) into your app.
  * Implement your queue message and serializer
  * Use the relevant client factory to get an instance to the queue driver
  * Optionally, inject your own PSR logger, like so: `$queue->setLogger($myLogger)`
  * Profit!

Notes
---

  * Each instance to the queue client is bound to one queue.
  * __AWS credentials:__ do not supply any if the IAM role of the node running your app is allowed in - this will typically
  be the case on queues/topics that live on the same account you're trying to publish/consume to.
  * __AWS endpoint:__ you only ever need to set this when you're using a third party SQS/SNS implementation, such as GoAWS.
