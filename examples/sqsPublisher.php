<?php
/**
 * This is an example on how to implement a queue publisher.
 */

use BBCWorldwide\Queue\Tests\Fixtures\Message;

require_once __DIR__ . '/../vendor/autoload.php';

$queue = include __DIR__ . '/makeSqsClient.php';

// Create a message
$message = new Message();
$message
    ->setFoo((string) random_int(0, 100000))
    ->setBar((string) random_int(0, 100000));

// ... and publish! The return value is an updated message updated with the message ID
$message = $queue->publish($message);

echo "\n Message sent:\n";
dump($message);
