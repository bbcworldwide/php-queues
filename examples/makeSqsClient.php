<?php
/**
 * This is an example on how to correctly instantiate a SQS queue client together with all dependencies that you should
 * set up on whatever service manager apparatus your application uses.
 */

use BBCWorldwide\Queue\Client\SQS\Factory;
use BBCWorldwide\Queue\Tests\Fixtures\Serializer;

require_once __DIR__ . '/../vendor/autoload.php';

$serializer = new Serializer();
$config     = include __DIR__ . '/awsCredentials.php';
$queue = Factory::getInstance(
    $serializer,
    null,
    $config['region'],
    $config['credentials']['key'],
    $config['credentials']['secret']
);

$queue->subscribe($config['sqsQueueName']);

return $queue;
