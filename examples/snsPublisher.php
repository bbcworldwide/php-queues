<?php
/**
 * This is an example on how to correctly instantiate a SNS queue client together with all dependencies that you should
 * set up on whatever service manager apparatus your application uses.
 */

use BBCWorldwide\Queue\Client\SNS\Factory;
use BBCWorldwide\Queue\Tests\Fixtures\Message;
use BBCWorldwide\Queue\Tests\Fixtures\Serializer;

require_once __DIR__ . '/../vendor/autoload.php';

/**
 * First of all, bootstrap dependencies - this example instantiates a SQS implementation of our
 * Queueing\ClientInterface; it needs a message serializer as well as an AWS SDK sqs client.
 *
 * The message serializer in turn uses Symfony's serializer.
 */

$serializer = new Serializer();
$config     = include __DIR__ . '/awsCredentials.php';
$queue = Factory::getInstance(
    $serializer,
    null,
    $config['region'],
    $config['credentials']['key'],
    $config['credentials']['secret']
);

$queue->subscribe($config['snsArn']);


// Create a message
$message = new Message();
$message
    ->setFoo((string) random_int(0, 100000))
    ->setBar((string) random_int(0, 100000));

// ... and publish! The return value is an updated message updated with the message ID
$message = $queue->publish($message);

echo "\n Message sent:\n";
dump($message);
