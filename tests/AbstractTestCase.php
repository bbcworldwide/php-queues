<?php
namespace BBCWorldwide\Queue\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Component\VarDumper\VarDumper;

/**
 * Base class for tests with a number of common data providers.
 *
 * @author BBC Worldwide
 */
class AbstractTestCase extends TestCase
{
    /**
     * Wrapper around symfony's var dumper
     *
     * @param array ...$args
     */
    public static function dump(...$args)
    {
        foreach ($args as $var) {
            VarDumper::dump($var);
        }
    }


    /***** Data providers *****/

    /**
     * Returns a list random strings.
     *
     * @return array
     */
    public function randomStringsDataProvider()
    {
        return [
            ['foo'],
            [uniqid(mt_rand(0, 1000000), true)],
            [(string) random_int(0, 10000)],
        ];
    }

    /**
     * A simple boolean data provider.
     *
     * @return array
     */
    public function booleanDataProvider()
    {
        return [
            [true],
            [false],
        ];
    }
}
