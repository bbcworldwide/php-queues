<?php

namespace BBCWorldwide\Tests\Queue\Client\SQS;

use Aws\CommandInterface;
use Aws\Result;
use Aws\Sqs\Exception\SqsException;
use Aws\Sqs\SqsClient;
use BBCWorldwide\Queue\Exception\PublishException;
use BBCWorldwide\Queue\Exception\PurgeException;
use BBCWorldwide\Queue\Exception\QueueingException;
use BBCWorldwide\Queue\Message\MessageInterface;
use BBCWorldwide\Queue\Client\SQS\Client;
use BBCWorldwide\Queue\Tests\Client\AbstractClientTestCase;

/**
 * IMPORTANT NOTE: AWS SDK client is evil as it uses magic methods via __call. There's a bug also on PHP7
 * whereby if you try to mock a particular __call to the client, and you don't specify exactly self::at(rightNumberHere)
 * you'll get a segfault. Doesn't happen on PHP5 though.
 *
 * @author BBC Worldwide
 */
class SqsClientTest extends AbstractClientTestCase
{
    /**
     * @var SqsClient|\PHPUnit_Framework_MockObject_MockObject
     */
    private $awsSqsClient;

    /**
     * @var Client
     */
    private $instance;

    const MOCK_QUEUE_URL = 'http://foo/bar';

    public function setUp()
    {
        parent::setUp();

        $this->awsSqsClient = $this->getMockBuilder(SqsClient::class)->disableOriginalConstructor()->getMock();

        $this->instance = new Client($this->awsSqsClient, $this->serializer);
        $this->instance->subscribe(self::MOCK_QUEUE_NAME);
    }

    public function tearDown()
    {
        parent::tearDown();

        $this->awsSqsClient = null;
        $this->instance     = null;
    }

    protected function setGetQueueUrlExpectation()
    {
        // First call is always to get the queue URL
        $result             = new Result();
        $result['QueueUrl'] = self::MOCK_QUEUE_URL;

        $this->awsSqsClient
            ->expects(self::at(0))
            ->method('__call')
            ->with('getQueueUrl')
            ->willReturn($result);
    }

    /**
     * @test
     * @expectedException \BBCWorldwide\Queue\Exception\QueueingException
     * @expectedExceptionMessage Queue name not configured
     */
    public function failWithNoConfiguredQueue()
    {
        $queue = new Client($this->awsSqsClient, $this->serializer);
        $queue->next();
    }

    /**
     * @test
     * @expectedException \BBCWorldwide\Queue\Exception\DeleteException
     */
    public function doSucceedThrowsDeleteException()
    {
        $exception     = $this->makeSqsException('foo');
        $receiptHandle = 'foobar';
        $message       = $this->getValidMessage();
        $message->addMetadata('receiptHandle', $receiptHandle);

        $this->setGetQueueUrlExpectation();
        $this->awsSqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('deleteMessage')
            ->willThrowException($exception);

        $this->instance->succeed($message);
    }

    /**
     * @test
     */
    public function doSucceedGetQueueUrlCreatesQueueIfNotExists()
    {
        $this->setGetQueueUrlExpectation();

        $exception     = $this->makeSqsException('foo', 'AWS.SimpleQueueService.NonExistentQueue');
        $receiptHandle = 'foobar';
        $message       = $this->getValidMessage();
        $message->addMetadata('receiptHandle', $receiptHandle);

        $sqsPayload = [
            'QueueUrl'      => self::MOCK_QUEUE_URL,
            'ReceiptHandle' => $receiptHandle,
        ];

        $this->awsSqsClient
            ->expects(self::at(0))
            ->method('__call')
            ->with('getQueueUrl')
            ->willThrowException($exception);

        // First call is always to get the queue URL
        $awsResult             = new Result();
        $awsResult['QueueUrl'] = 'http://foo/bar';

        // This is it
        $this->awsSqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('createQueue', [['QueueName' => self::MOCK_QUEUE_NAME]])
            ->willReturn($awsResult);

        // Don't care about the rest
        $this->awsSqsClient
            ->expects(self::at(2))
            ->method('__call')
            ->with('deleteMessage', [$sqsPayload]);

        $this->instance->succeed($message);
    }

    /**
     * @test
     */
    public function doSucceedGetQueueReconvertsExceptionOnUnhandledSqsError()
    {
        $this->setGetQueueUrlExpectation();

        $exceptionMessage = 'barfoobar';
        $exception        = $this->makeSqsException($exceptionMessage);

        $message = $this->getValidMessage();
        $message->addMetadata('receiptHandle', 'foo');

        $this->awsSqsClient
            ->expects(self::at(0))
            ->method('__call')
            ->with('getQueueUrl')
            ->willThrowException($exception);

        $this->expectException(QueueingException::class);
        $this->expectExceptionMessage($exceptionMessage);

        $this->instance->succeed($message);
    }

    /**
     * @test
     * @expectedException \BBCWorldwide\Queue\Exception\ReadException
     */
    public function nextFailsWithReadException()
    {
        $this->setGetQueueUrlExpectation();

        $this->awsSqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('receiveMessage')
            ->willThrowException($this->makeSqsException());

        self::assertFalse($this->instance->next());
    }

    /**
     * @test
     */
    public function nextOnEmptyQueue()
    {
        $this->setGetQueueUrlExpectation();

        // Make SQS return nothing
        $sqsResult = new Result();

        $this->awsSqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('receiveMessage')
            ->willReturn($sqsResult);

        self::assertFalse($this->instance->next());
    }

    /**
     * @test
     */
    public function nextSucceeds()
    {
        $this->setGetQueueUrlExpectation();

        // Message mock data
        $messageId      = 'foobar';
        $receiptHandle  = 'barfoo';
        $body           = 'fakejson';
        $messagePayload = [
            [
                'Body'              => $body,
                'MessageId'         => $messageId,
                'ReceiptHandle'     => $receiptHandle,
                'MessageAttributes' => [
                    'foo' => [
                        'DataType'    => 'String',
                        'StringValue' => 'bar',
                    ],
                ],
            ],
        ];

        // Add message attribute from above
        $message = $this->getValidMessage();
        $message->addMetadata('foo', 'bar');

        // Make SQS return message
        $sqsResult             = new Result();
        $sqsResult['Messages'] = $messagePayload;

        $this->awsSqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('receiveMessage')
            ->willReturn($sqsResult);

        $this->serializer
            ->expects(self::once())
            ->method('deserialize')
            ->with($body)
            ->willReturn($message);

        // Check we're getting the same message instance
        self::assertSame($message, $this->instance->next());

        // Check values set dynamically
        self::assertSame($messageId, $message->getMessageId());
        self::assertSame($receiptHandle, $message->getMetadata('receiptHandle'));
    }

    /**
     * @test
     */
    public function publishHandlesSqsException()
    {
        $this->setGetQueueUrlExpectation();

        $exceptionMessage = 'asasdasdbarfoo';

        $exception = $this->makeSqsException($exceptionMessage);

        $this->expectException(PublishException::class);
        $this->expectExceptionMessage($exceptionMessage);

        $this->awsSqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('sendMessage')
            ->willThrowException($exception);

        $this->instance->publish($this->getValidMessage());
    }

    /**
     * @test
     * @dataProvider validMessagesDataProvider
     */
    public function publishSucceeds(MessageInterface $message, array $messageAttributes)
    {
        $this->setGetQueueUrlExpectation();

        $serializedMessage = 'foobarfoo';
        $messageId         = 'asdasd';
        $messagePayload    = [
            'QueueUrl'          => self::MOCK_QUEUE_URL,
            'MessageBody'       => $serializedMessage,
            'MessageAttributes' => $messageAttributes,
        ];

        $resultModel              = new Result();
        $resultModel['MessageId'] = $messageId;

        $this->serializer
            ->expects(self::once())
            ->method('serialize')
            ->with($message)
            ->willReturn($serializedMessage);

        $this->awsSqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('sendMessage', [$messagePayload])
            ->willReturn($resultModel);

        // Checking it's the same instance and type
        self::assertSame($message, $this->instance->publish($message));

        // Also check messageId is there
        self::assertSame($messageId, $message->getMessageId());
    }

    /**
     * @test
     * @dataProvider validMessagesDataProvider
     */
    public function publishRemovesReceiptHandle(MessageInterface $message, array $expectedMessageAttributes)
    {
        $this->setGetQueueUrlExpectation();

        $key = Client::RECEIPT_HANDLE_KEY;

        $resultModel              = new Result();
        $resultModel['MessageId'] = 'bar';

        // Add bogus receipt handle to message, make sure it's in there
        $receiptHandle = 'lerele';
        $message->addMetadata($key, $receiptHandle);
        self::assertSame($receiptHandle, $message->getMetadata($key));

        // Now, intercept message and make sure receipt handle has been taken away
        $this->serializer
            ->expects(self::once())
            ->method('serialize')
            ->with($message)
            ->willReturn('foobarfoo');

        $this->awsSqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('sendMessage', self::callback(function (array $payload) use ($expectedMessageAttributes) {
                self::assertArrayHasKey('MessageAttributes', $payload[0]);
                $messageAttributes = $payload[0]['MessageAttributes'];

                self::assertEquals($expectedMessageAttributes, $messageAttributes);
                self::assertArrayNotHasKey('receiptHandle', $messageAttributes);

                return true;
            }))
            ->willReturn($resultModel);

        self::assertSame($message, $this->instance->publish($message));
    }

    /**
     * @test
     */
    public function purgeSucceeds()
    {
        $this->setGetQueueUrlExpectation();

        $this->awsSqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('purgeQueue');

        $this->instance->purge();
    }

    /**
     * @test
     */
    public function purgeHandlesSqsException()
    {
        $this->setGetQueueUrlExpectation();

        $exceptionMessage = 'asasdasdbarfoo';

        $exception = $this->makeSqsException($exceptionMessage);

        $this->expectException(PurgeException::class);
        $this->expectExceptionMessage($exceptionMessage);

        $this->awsSqsClient
            ->expects(self::at(1))
            ->method('__call')
            ->with('purgeQueue')
            ->willThrowException($exception);

        $this->instance->purge();
    }

    private function makeSqsException($message = 'foo', $code = 'bar')
    {
        /** @var CommandInterface $command */
        $command = $this->getMockBuilder(CommandInterface::class)->getMock();

        return new SqsException($message, $command, ['code' => $code]);
    }
}
