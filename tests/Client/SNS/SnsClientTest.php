<?php

namespace BBCWorldwide\Queue\Tests\Client\AWS;

use Aws\CommandInterface;
use Aws\Result;
use Aws\Sns\Exception\SnsException;
use Aws\Sns\SnsClient;
use BBCWorldwide\Queue\Client\SNS\Client;
use BBCWorldwide\Queue\Exception\PublishException;
use BBCWorldwide\Queue\Message\MessageInterface;
use BBCWorldwide\Queue\Tests\Client\AbstractClientTestCase;
use BBCWorldwide\Queue\Tests\Fixtures\Message;

class SnsClientTest extends AbstractClientTestCase
{
    /**
     * @var SnsClient|\PHPUnit_Framework_MockObject_MockObject
     */
    private $snsClient;

    /**
     * @var Client
     */
    private $instance;

    const MOCK_QUEUE_NAME = 'arn:aws:sns:local:000000000000:local-topic1';

    public function setUp()
    {
        parent::setUp();

        $this->snsClient = $this->getMockBuilder(SnsClient::class)->disableOriginalConstructor()->getMock();

        $this->instance = new Client($this->snsClient, $this->serializer);
        $this->instance->subscribe(self::MOCK_QUEUE_NAME);
    }

    public function tearDown()
    {
        parent::tearDown();

        $this->snsClient  = null;
        $this->serializer = null;
        $this->instance   = null;
    }

    /***** Reading from the queue is not supported on this driver, we can only publish *****/

    /**
     * @test
     * @expectedException \BBCWorldwide\Queue\Exception\UnsupportedOperationException
     */
    public function nextThrowsUnsupportedOperationException()
    {
        $this->instance->next();
    }

    /**
     * @test
     * @expectedException \BBCWorldwide\Queue\Exception\UnsupportedOperationException
     */
    public function succeedThrowsUnsupportedOperationException()
    {
        $this->instance->succeed(new Message());
    }

    /**
     * @test
     * @expectedException \BBCWorldwide\Queue\Exception\UnsupportedOperationException
     */
    public function purgeThrowsUnsupportedOperationException()
    {
        $this->instance->purge();
    }

    /**
     * @test
     * @expectedException \BBCWorldwide\Queue\Exception\QueueingException
     * @expectedExceptionMessage Queue name not configured
     */
    public function failWithNoConfiguredQueue()
    {
        $message = new Message();
        $message
            ->setFoo('gah')
            ->setBar('lah');

        $queue = new Client($this->snsClient, $this->serializer);
        $queue->publish($message);
    }

    /**
     * @test
     * @dataProvider             randomStringsDataProvider
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage queueName must be an AWS arn
     */
    public function publishFailsOnDodgyQueueName($notAnArn)
    {
        $this->instance
            ->subscribe($notAnArn)
            ->publish($this->getValidMessage());
    }

    /**
     * @test
     */
    public function publishHandlesSqsException()
    {
        $exceptionMessage = 'asasdasdbarfoo';

        /** @var CommandInterface $command */
        $command   = $this->getMockBuilder(CommandInterface::class)->getMock();
        $exception = new SnsException($exceptionMessage, $command, ['code' => 'foobar']);

        $this->expectException(PublishException::class);
        $this->expectExceptionMessage($exceptionMessage);

        $this->snsClient
            ->expects(self::once())
            ->method('__call')
            ->with('publish')
            ->willThrowException($exception);

        $this->instance->publish($this->getValidMessage());
    }

    /**
     * @test
     * @dataProvider validMessagesDataProvider
     */
    public function publishSucceeds(MessageInterface $message, array $messageAttributes)
    {
        $serializedMessage = 'foobarfoo';
        $messageId         = 'asdasd';

        $messagePayload = [
            'TopicArn'          => self::MOCK_QUEUE_NAME,
            'Message'           => $serializedMessage,
            'MessageAttributes' => $messageAttributes,
        ];

        $resultModel              = new Result();
        $resultModel['MessageId'] = $messageId;

        $this->serializer
            ->expects(self::once())
            ->method('serialize')
            ->with($message)
            ->willReturn($serializedMessage);

        $this->snsClient
            ->expects(self::once())
            ->method('__call')
            ->with('publish', [$messagePayload])
            ->willReturn($resultModel);

        // Checking it's the same instance and type
        self::assertSame($message, $this->instance->publish($message));

        // Also check messageId is there
        self::assertSame($messageId, $message->getMessageId());
    }
}
