<?php

namespace BBCWorldwide\Queue\Tests\Client;

use BBCWorldwide\Queue\Tests\Fixtures\Message;
use BBCWorldwide\Queue\Message\SerializerInterface;
use BBCWorldwide\Queue\Tests\AbstractTestCase;

/**
 * Base class for client tests
 *
 * @author BBC Worldwide
 */
abstract class AbstractClientTestCase extends AbstractTestCase
{
    const MOCK_QUEUE_NAME = 'foobar-queue';

    /**
     * @var SerializerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $serializer;

    public function setUp()
    {
        parent::setUp();

        $this->serializer = $this
            ->getMockBuilder(SerializerInterface::class)
            ->getMock();
    }

    public function tearDown()
    {
        parent::tearDown();

        $this->serializer = null;
    }

    /**
     * Returns a message which can self validate
     *
     * @return Message
     */
    protected function getValidMessage()
    {
        $message = new Message();
        $message
            ->setFoo(md5(mt_rand(0, 874354657)))
            ->setBar(md5(mt_rand(0, 564631321)));

        return $message;
    }

    /**
     * Returns a message which can self validate
     *
     * @return array
     */
    public function validMessagesDataProvider()
    {
        $messageA              = $this->getValidMessage();
        $sqsMessageAttributesA = [];

        $messageB = $this->getValidMessage();
        $messageB->addMetadata('gah', 'blud');
        $sqsMessageAttributesB = [
            'gah' => [
                'DataType'    => 'String',
                'StringValue' => 'blud',
            ],
        ];

        return [
            [
                $messageA,
                $sqsMessageAttributesA,
            ],
            [
                $messageB,
                $sqsMessageAttributesB,
            ],
        ];
    }

}
