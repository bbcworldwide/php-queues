<?php

namespace BBCWorldWide\Queue\Tests\Helper;

use Aws\Credentials\Credentials;
use BBCWorldwide\Queue\Helper\AWS\AwsConfigTrait;
use BBCWorldwide\Queue\Tests\AbstractTestCase;

class AwsConfigTraitTestClass
{
    use AwsConfigTrait;

    /**
     * Method being tested is protected, this allows us to call it from tests.
     */
    public static function makeConfigPassthrough(
        $awsRegion = 'eu-west-1',
        $awsKey = null,
        $awsSecret = null,
        $endpoint = null
    ) {
        return self::makeConfig($awsRegion, $awsKey, $awsSecret, $endpoint);
    }
}

class AwsConfigTraitTest extends AbstractTestCase
{
    /**
     * @var array
     */
    private $exampleConfig = ['version' => 'latest', 'region' => 'eu-west-1'];

    /**
     * @test
     */
    public function makeConfigWithoutAnyParams()
    {
        $expectedConfig = $this->exampleConfig;
        self::assertEquals($expectedConfig, AwsConfigTraitTestClass::makeConfigPassthrough());
    }

    /**
     * @test
     */
    public function makeConfigWithSomeOtherRegion()
    {
        $region                   = 'foobar';
        $expectedConfig           = $this->exampleConfig;
        $expectedConfig['region'] = $region;
        self::assertEquals($expectedConfig, AwsConfigTraitTestClass::makeConfigPassthrough($region));
    }

    /**
     * @test
     */
    public function makeConfigWithoutAllCredentials()
    {
        $expectedConfig = $this->exampleConfig;
        $region         = $expectedConfig['region'];

        $config = AwsConfigTraitTestClass::makeConfigPassthrough($region, 'foo');
        self::assertEquals($expectedConfig, $config);

        $config = AwsConfigTraitTestClass::makeConfigPassthrough($region, null, 'bar');
        self::assertEquals($expectedConfig, $config);
    }

    /**
     * @test
     */
    public function makeConfigWithCredentials()
    {
        $expectedConfig = $this->exampleConfig;
        $region         = $expectedConfig['region'];

        $expectedConfig['credentials'] = new Credentials('foo', 'bar');

        $config = AwsConfigTraitTestClass::makeConfigPassthrough($region, 'foo', 'bar');
        self::assertEquals($expectedConfig, $config);
    }

    /**
     * @test
     */
    public function makeConfigWithTheWholeThing()
    {
        $expectedConfig = $this->exampleConfig;
        $region         = $expectedConfig['region'];
        $endpoint       = 'stuff';

        $expectedConfig['credentials'] = new Credentials('foo', 'bar');
        $expectedConfig['endpoint']    = $endpoint;

        $config = AwsConfigTraitTestClass::makeConfigPassthrough($region, 'foo', 'bar', $endpoint);
        self::assertEquals($expectedConfig, $config);
    }

}
