<?php

namespace BBCWorldwide\Queue\Tests\Fixtures;

use BBCWorldwide\Queue\Helper\CoalesceTrait;
use BBCWorldwide\Queue\Message\MessageInterface;
use BBCWorldwide\Queue\Message\SerializerInterface;

class Serializer implements SerializerInterface
{
    use CoalesceTrait;

    /**
     * @inheritdoc
     */
    public function serialize(MessageInterface $message)
    {
        return json_encode($message->summary());
    }

    /**
     * @inheritdoc
     */
    public function deserialize($value)
    {
        $decoded = json_decode($value);

        return (new Message())
            ->setFoo($this->coalesce($decoded, 'foo'))
            ->setBar($this->coalesce($decoded, 'bar'));
    }
}
