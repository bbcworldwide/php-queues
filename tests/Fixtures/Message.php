<?php

namespace BBCWorldwide\Queue\Tests\Fixtures;

use BBCWorldwide\Queue\Message\AbstractMessage;
use BBCWorldwide\Queue\Message\MessageInterface;

class Message extends AbstractMessage implements MessageInterface
{
    /**
     * @var string
     */
    protected $foo;

    /**
     * @var string
     */
    protected $bar;

    /**
     * @return string
     */
    public function getFoo()
    {
        return $this->foo;
    }

    /**
     * @param string $foo
     *
     * @return Message
     */
    public function setFoo($foo)
    {
        $this->foo = $foo;

        return $this;
    }

    /**
     * @return string
     */
    public function getBar()
    {
        return $this->bar;
    }

    /**
     * @param string $bar
     *
     * @return Message
     */
    public function setBar($bar)
    {
        $this->bar = $bar;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function selfValidate()
    {
        if ($this->foo === null) {
            throw new \InvalidArgumentException('Foo cannot be null');
        }

        if ($this->bar === null) {
            throw new \InvalidArgumentException('Bar cannot be null');
        }
    }

    /**
     * @inheritdoc
     */
    public function summary()
    {
        return [
            'foo' => $this->foo,
            'bar' => $this->bar,
        ];
    }
}
