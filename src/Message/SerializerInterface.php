<?php
namespace BBCWorldwide\Queue\Message;

/**
 * Interface SerializerInterface
 *
 * @author BBC WorldWide
 */
interface SerializerInterface
{
    /**
     * Serializes a message into our chosen format.
     *
     * @param MessageInterface $message
     *
     * @return string
     */
    public function serialize(MessageInterface $message);

    /**
     * Deserializes a string value into a message.
     *
     * @param string $value
     *
     * @return MessageInterface
     */
    public function deserialize($value);
}
