<?php

namespace BBCWorldwide\Queue\Message;

use BBCWorldwide\Queue\Helper\CoalesceTrait;
use BBCWorldwide\Queue\Helper\Exception\ValidationError;
use BBCWorldwide\Queue\Helper\NonEmptyStringValidator;

/**
 * Common message logic and state.
 *
 * @author BBC Worldwide
 */
abstract class AbstractMessage
{
    use CoalesceTrait;

    /**
     * @var string
     */
    private $messageId;

    /**
     * Store any other message information related to the specific queue implementation in here
     *
     * @var array
     */
    private $metadata = [];

    /**
     * Returns the message id - this will be whichever unique identifier applies to a single message on the
     * queue implementation.
     *
     * @return string
     */
    public function getMessageId()
    {
        return $this->messageId;
    }

    /**
     * Sets the message id.
     *
     * @param string $messageId
     *
     * @return self
     * @throws ValidationError
     */
    public function setMessageId($messageId)
    {
        if ($messageId !== null) {
            NonEmptyStringValidator::validate($messageId);
        }

        $this->messageId = $messageId;

        return $this;
    }

    /**
     * Adds an entry to the metadata stack, by name.
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return self
     * @throws ValidationError
     */
    public function addMetadata($name, $value)
    {
        NonEmptyStringValidator::validate($name);

        $this->metadata[$name] = $value;

        return $this;
    }

    /**
     * Removes a given metadata entry, if it exists.
     *
     * @param string $name
     *
     * @return bool
     */
    public function removeMetadata($name)
    {
        if (array_key_exists($name, $this->metadata) === true) {
            unset($this->metadata[$name]);

            return true;
        }

        return false;
    }

    /**
     * Gets a specific value off the metadata stack. Returns null if it doesn't exist.
     *
     * @param string $name
     *
     * @return mixed|null
     * @throws ValidationError
     */
    public function getMetadata($name)
    {
        NonEmptyStringValidator::validate($name);

        return $this->coalesce($this->metadata, $name, null);
    }

    /**
     * Returns the whole metadata array.
     *
     * @return array
     */
    public function getAllMetadata()
    {
        return $this->metadata;
    }
}
