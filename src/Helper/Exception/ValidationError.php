<?php
namespace BBCWorldwide\Queue\Helper\Exception;

/**
 * Validation error exception.
 *
 * @author BBC Worldwide
 */
class ValidationError extends \RuntimeException
{
}
