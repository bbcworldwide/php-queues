<?php

namespace BBCWorldwide\Queue\Helper\AWS;

use BBCWorldwide\Queue\Message\MessageInterface;

/**
 * Common code to handling SQS and SNS.
 *
 * @author BBC Worldwide
 */
trait MakeMessageAttributesTrait
{
    /**
     * Build SQS/SNS message attributes array off the message metadata.
     *
     * @param MessageInterface $message
     *
     * @return array
     */
    protected function makeMessageAttributes(MessageInterface $message)
    {
        // Compose MessageAttributes as per SQS structure
        $messageAttributes = [];
        foreach ($message->getAllMetadata() as $name => $value) {
            $messageAttributes[$name] = [
                'DataType'    => 'String',
                'StringValue' => $value,
            ];
        }

        return $messageAttributes;
    }
}
