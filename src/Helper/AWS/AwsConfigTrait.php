<?php

namespace BBCWorldwide\Queue\Helper\AWS;

use Aws\Credentials\Credentials;

/**
 * Use to make an AWS config array we can pass on to AWS service constructors.
 *
 * @author BBC WorldWide
 */
trait AwsConfigTrait
{
    /**
     * Returns an array that can be used in AWS SDK constructors to instantiate clients. Allows for IAM
     * on nodes which have already access to services, by not specifying key/secrets.
     *
     * @param string      $awsRegion AWS region the queue lives at
     * @param string|null $awsKey    AWS key
     * @param string|null $awsSecret AWS secret
     * @param string|null $endpoint  AWS endpoint (not queue URL)
     *
     * @return array
     */
    protected static function makeConfig($awsRegion = 'eu-west-1', $awsKey = null, $awsSecret = null, $endpoint = null)
    {
        $config = [
            'version' => 'latest',
            'region'  => $awsRegion,
        ];

        if ($awsKey !== null && $awsSecret !== null) {
            $config['credentials'] = new Credentials($awsKey, $awsSecret);
        }

        if ($endpoint !== null) {
            $config['endpoint'] = $endpoint;
        }

        return $config;
    }
}
