<?php
namespace BBCWorldwide\Queue\Exception;

/**
 * Thrown when there's a problemo purging items from the queue.
 *
 * @author BBC Worldwide
 */
class PurgeException extends QueueingException
{

}
