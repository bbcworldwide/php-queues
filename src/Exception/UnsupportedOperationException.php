<?php
namespace BBCWorldwide\Queue\Exception;

/**
 * Thrown when a queue operation is unsupported.
 *
 * @author BBC Worldwide
 */
class UnsupportedOperationException extends QueueingException
{

}
