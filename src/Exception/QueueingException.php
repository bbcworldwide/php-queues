<?php
namespace BBCWorldwide\Queue\Exception;

/**
 * Generic, catch all exception for queuing issues.
 *
 * @author BBC Worldwide
 */
class QueueingException extends \Exception
{

}
