<?php

namespace BBCWorldwide\Queue\Client;

use BBCWorldwide\Queue\Exception;
use BBCWorldwide\Queue\Helper\Exception\ValidationError;
use BBCWorldwide\Queue\Helper\NonEmptyStringValidator;
use BBCWorldwide\Queue\Message\MessageInterface as Message;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;

/**
 * Common logic to all queues.
 *
 * This class is PSR Logger Aware.
 *
 * Each instance is bound to just one queue. Use setQueueName accordingly.
 *
 * @author BBC WorldWide
 */
abstract class AbstractClient implements ClientInterface
{
    use LoggerAwareTrait;

    /**
     * Use getSubscribedQueue to get to this from child classes.
     *
     * @var string
     */
    private $subscribedQueue;

    /**
     * Implements actually getting a message from the queue.
     *
     * @return Message
     *
     * @throws Exception\EmptyQueueException    When no message was available on the queue.
     * @throws Exception\ReadException          When unable to read from queue
     */
    abstract protected function doGetNext();

    /**
     * Implement here whatever needs doing on the queue when a message has been processed successfully.
     *
     * @param Message $message
     *
     * @return void
     *
     * @throws Exception\DeleteException
     */
    abstract protected function doSucceed(Message $message);

    /**
     * Implement here publishing of the message into the queue. Must return the messageId (if any), or null if not
     * applicable.
     *
     * @param Message $message
     *
     * @return string|null
     * @throws Exception\PublishException
     */
    abstract protected function doPublish(Message $message);

    /**
     * Implement here purging items from the queue.
     *
     * @throws Exception\PurgeException
     */
    abstract protected function doPurge();

    /**
     * Sets a default logger (null driver).
     */
    public function __construct()
    {
        $this->setLogger(new NullLogger());
    }

    /**
     * @inheritdoc
     */
    public function next()
    {
        try {
            $message = $this->doGetNext();
        } catch (Exception\EmptyQueueException $ex) {
            $this->logger->info(
                sprintf('Empty response from queue %s', $this->subscribedQueue),
                [
                    'type'      => 'queue-read',
                    'success'   => true,
                    'queueName' => $this->subscribedQueue,
                    'empty'     => true,
                ]
            );

            return false;
        } catch (Exception\ReadException $ex) {
            $this->logger->error(
                sprintf('Error reading from queue %s', $this->subscribedQueue),
                [
                    'type'      => 'queue-read',
                    'success'   => false,
                    'queueName' => $this->subscribedQueue,
                    'err'       => $ex,
                ]
            );
            throw $ex;
        }

        $this->logger->info(
            sprintf('Received message from queue %s', $this->subscribedQueue),
            [
                'type'           => 'queue-read',
                'success'        => true,
                'queueName'      => $this->subscribedQueue,
                'empty'          => false,
                'messageId'      => $message->getMessageId(),
                'messageSummary' => $message->summary(),
            ]
        );

        return $message;
    }

    /**
     * @inheritdoc
     */
    public function succeed(Message $message)
    {
        try {
            $this->doSucceed($message);
        } catch (Exception\DeleteException $ex) {
            $this->logger->error(
                sprintf('Error deleting message from queue %s', $this->subscribedQueue),
                [
                    'type'      => 'queue-ack',
                    'success'   => false,
                    'queueName' => $this->subscribedQueue,
                    'messageId' => $message->getMessageId(),
                ]
            );
            throw $ex;
        }

        $this->logger->info(
            sprintf('Deleted message from queue %s', $this->subscribedQueue),
            [
                'type'      => 'queue-ack',
                'success'   => true,
                'queueName' => $this->subscribedQueue,
                'messageId' => $message->getMessageId(),
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function publish(Message $message)
    {
        // Ensure message is valid before sending
        $message->selfValidate();

        try {
            // Send off to queue driver for publishing,
            $messageId = $this->doPublish($message);
        } catch (Exception\PublishException $ex) {
            $this->logger->error(
                sprintf('Error publishing message to the queue %s', $this->subscribedQueue),
                [
                    'type'           => 'queue-write',
                    'success'        => false,
                    'queueName'      => $this->subscribedQueue,
                    'messageId'      => $message->getMessageId(),
                    'messageSummary' => $message->summary(),
                    'err'            => $ex,
                ]
            );
            throw $ex;
        }

        // Re-set message ID if provided
        $message->setMessageId($messageId);

        $this->logger->info(
            sprintf('Published message to the queue %s', $this->subscribedQueue),
            [
                'type'           => 'queue-write',
                'success'        => true,
                'queueName'      => $this->subscribedQueue,
                'messageId'      => $message->getMessageId(),
                'messageSummary' => $message->summary(),
            ]
        );

        return $message;
    }

    /**
     * @inheritdoc
     */
    public function purge()
    {
        try {
            $this->doPurge();
        } catch (Exception\PurgeException $ex) {
            $this->logger->error(
                sprintf('Error purging the queue %s', $this->subscribedQueue),
                [
                    'type'      => 'queue-purge',
                    'success'   => false,
                    'queueName' => $this->subscribedQueue,
                    'err'       => $ex,
                ]
            );
            throw $ex;
        }

        $this->logger->info(
            sprintf('Purged the queue %s', $this->subscribedQueue),
            [
                'type'      => 'queue-purge',
                'success'   => true,
                'queueName' => $this->subscribedQueue,
            ]
        );

        return $this;
    }

    /**
     * Each instance is bound to one single queue/topic. Provide it here.
     *
     * @param string $queueName
     *
     * @return $this
     *
     * @throws ValidationError
     */
    public function subscribe($queueName)
    {
        NonEmptyStringValidator::validate($queueName);

        $this->subscribedQueue = $queueName;

        return $this;
    }

    /**
     * Ensures we are bound to a queue before returning its name. queueName is private for this reason.
     *
     * @return string
     * @throws Exception\QueueingException
     */
    protected function getSubscribedQueue()
    {
        if ($this->subscribedQueue === null) {
            throw new Exception\QueueingException('Queue name not configured, please provide with one. Or not.');
        }

        return $this->subscribedQueue;
    }
}
