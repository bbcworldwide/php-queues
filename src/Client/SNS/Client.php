<?php

namespace BBCWorldwide\Queue\Client\SNS;

use Aws\Sns\Exception\SnsException;
use Aws\Sns\SnsClient;
use BBCWorldwide\Queue\Client\AbstractClient;
use BBCWorldwide\Queue\Exception;
use BBCWorldwide\Queue\Helper\AWS\MakeMessageAttributesTrait;
use BBCWorldwide\Queue\Message\MessageInterface as Message;
use BBCWorldwide\Queue\Message\SerializerInterface as Serializer;
use BBCWorldwide\Queue\Client\SQS\Client as SQS;

/**
 * AWS SNS queue client.
 *
 * Ensure you set the queue name to the topic arn.
 *
 * @author BBC Worldwide
 */
class Client extends AbstractClient
{
    use MakeMessageAttributesTrait;

    /**
     * @var SnsClient
     */
    private $snsClient;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * Ensure a AWS SDK SNS client is passed in here. It must already be provisioned with AWS credentials and
     * generally ready to use.
     *
     * @param SnsClient  $snsClient
     * @param Serializer $serializer
     */
    public function __construct(SnsClient $snsClient, Serializer $serializer)
    {
        parent::__construct();

        $this->snsClient  = $snsClient;
        $this->serializer = $serializer;
    }

    /**
     * Partially overriding to ensure it's an AWS arn.
     *
     * @return string
     * @throws \InvalidArgumentException
     */
    protected function getSubscribedQueue()
    {
        $arn = parent::getSubscribedQueue();

        if (preg_match('/^arn\:aws\:sns\:(.*)\:\d{12}\:(.*)$/', $arn) === 0) {
            throw new \InvalidArgumentException(sprintf('queueName must be an AWS arn'));
        }

        return $arn;
    }

    /**
     * @inheritdoc
     */
    protected function doPublish(Message $message)
    {
        try {
            // Remove receipt handle in those cases where we're getting a message bounced back from SQS into the topic
            $message->removeMetadata(SQS::RECEIPT_HANDLE_KEY);

            return $this->snsClient->publish([
                'TopicArn'          => $this->getSubscribedQueue(),
                'Message'           => $this->serializer->serialize($message),
                'MessageAttributes' => $this->makeMessageAttributes($message),
            ])->get('MessageId');
        } catch (SnsException $ex) {
            throw new Exception\PublishException($ex->getMessage(), $ex->getCode(), $ex);
        }
    }

    /**
     * @inheritdoc
     */
    protected function doPurge()
    {
        throw new Exception\UnsupportedOperationException('SNS does not support purge');
    }

    /**
     * This is not supported on SNS.
     *
     * @inheritdoc
     * @throws Exception\UnsupportedOperationException
     */
    protected function doGetNext()
    {
        throw new Exception\UnsupportedOperationException('SNS does not support message retrieval');
    }

    /**
     * This is not supported on SNS.
     *
     * @inheritdoc
     * @throws Exception\UnsupportedOperationException
     */
    protected function doSucceed(Message $message)
    {
        throw new Exception\UnsupportedOperationException('SNS does not support message retrieval');
    }
}
