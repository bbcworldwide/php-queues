<?php

namespace BBCWorldwide\Queue\Client\SNS;

use Aws\Sns\SnsClient;
use BBCWorldwide\Queue\Helper\AWS\AwsConfigTrait;
use BBCWorldwide\Queue\Message\SerializerInterface;
use Psr\Log\LoggerInterface;

/**
 * Factory to create the SNS driver.
 *
 * @author BBC WorldWide
 * @codeCoverageIgnore
 */
class Factory
{
    use AwsConfigTrait;

    /**
     * Return a fully configured SQS driver given dependencies and aws credentials.
     *
     * AWS credentials are optional - do not use when the node your app is running is IAM-allowed in.
     *
     * @param SerializerInterface $serializer Instance to a message serializer
     * @param LoggerInterface     $logger     Instance to a logger
     * @param string|null         $topicArn   Topic ARN to subscribe to
     * @param string              $awsRegion  AWS region the queue lives at
     * @param string|null         $awsKey     AWS key
     * @param string|null         $awsSecret  AWS secret
     * @param string|null         $endpoint   AWS endpoint (not queue URL)
     *
     * @return \BBCWorldwide\Queue\Client\SNS\Client
     */
    public static function getInstance(
        SerializerInterface $serializer,
        LoggerInterface $logger = null,
        $topicArn = null,
        $awsRegion = 'eu-west-1',
        $awsKey = null,
        $awsSecret = null,
        $endpoint = null
    ) {
        $awsConfig = self::makeConfig($awsRegion, $awsKey, $awsSecret, $endpoint);

        $client = new Client(new SnsClient($awsConfig), $serializer);

        if ($logger !== null) {
            $client->setLogger($logger);
        }

        if ($topicArn !== null) {
            $client->subscribe($topicArn);
        }

        return $client;
    }
}
