<?php

namespace BBCWorldwide\Queue\Client;

use BBCWorldwide\Queue\Message\MessageInterface as Message;
use Psr\Log\LoggerAwareTrait;

/**
 * Null queue that doesn't do anything.
 *
 * @author BBC Worldwide
 * @codeCoverageIgnore
 */
class NullClient implements ClientInterface
{
    use LoggerAwareTrait;

    /**
     * @inheritdoc
     */
    public function next()
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function succeed(Message $message)
    {
    }

    /**
     * @inheritdoc
     */
    public function publish(Message $message)
    {
        return $message;
    }

    /**
     * @inheritdoc
     */
    public function subscribe($queue)
    {
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function purge()
    {
        return $this;
    }
}
